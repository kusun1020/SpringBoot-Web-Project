package com.daonplace.springbootweb.domain.user;

public enum WriteStatus {
    WRITE,
    CANCEL
}
