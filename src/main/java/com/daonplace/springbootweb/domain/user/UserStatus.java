package com.daonplace.springbootweb.domain.user;

public enum UserStatus {
    REGISTERED,
    WITHDRAWN,
    DORMANT
}
