package com.daonplace.springbootweb.domain.admin;

public enum PublishStatus {
    PUBLISH,
    CANCEL
}
